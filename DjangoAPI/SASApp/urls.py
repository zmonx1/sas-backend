from django.conf.urls import url
from SASApp import views

urlpatterns=[
    url(r'^course$',views.courseApi),
    url(r'^course/([0-9]+)$',views.courseApi),
    url(r'^user$',views.userApi),
    url(r'^user/([0-9]+)$',views.userApi),
    url(r'^userType$',views.userTypeApi),
    url(r'^userType/([0-9]+)$',views.userTypeApi),

    
]