from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from SASApp.models import Course,UserType,User
from SASApp.serializers import CourseSerializer,UserSerializer,UserTypeSerializer
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from SASApp.models import Course
from SASApp.serializers import CourseSerializer

from django.core.files.storage import default_storage

# Create your views here.

@csrf_exempt
def courseApi(request,id=0):
    if request.method=='GET':
        courses = Course.objects.all()
        course_serializer=CourseSerializer(courses,many=True)
        return JsonResponse(course_serializer.data,safe=False)
    elif request.method=='POST':
        course_data=JSONParser().parse(request)
        course_serializer=CourseSerializer(data=course_data)
        if course_serializer.is_valid():
            course_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add",safe=False)
    elif request.method=='PUT':
        course_data=JSONParser().parse(request)
        course=Course.objects.get(_Id=course_data['_id'])
        course_serializer=CourseSerializer(course,data=course_data)
        if course_serializer.is_valid():
            course_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update")
    elif request.method=='DELETE':
        course=Course.objects.get(_Id=id)
        course.delete()
        return JsonResponse("Deleted Successfully",safe=False)
@csrf_exempt
def SaveFile(request):
    file=request.FILES['file']
    file_name=default_storage.save(file.name,file)
    return JsonResponse(file_name,safe=False)

#----------------------- userType ------------------------------------------

@csrf_exempt
def userTypeApi(request,id=0):
    if request.method=='GET':
        userTypes = UserType.objects.all()
        userType_serializer= UserTypeSerializer(userTypes,many=True)
        return JsonResponse(userType_serializer.data,safe=False)
    elif request.method=='POST':
        userType_data=JSONParser().parse(request)
        userType_serializer=UserTypeSerializer(data=userType_data)
        if userType_serializer.is_valid():
            userType_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add",safe=False)
    elif request.method=='PUT':
        userType_data=JSONParser().parse(request)
        course=UserType.objects.get(_Id=userType_data['_id'])
        userType_serializer=UserTypeSerializer(course,data=userType_data)
        if userType_serializer.is_valid():
            userType_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update")
    elif request.method=='DELETE':
        course=UserType.objects.get(_Id=id)
        course.delete()
        return JsonResponse("Deleted Successfully",safe=False)

#----------------------- user ------------------------------------------

@csrf_exempt
def userApi(request,id=0):
    if request.method=='GET':
        user = User.objects.all()
        user_serializer= UserSerializer(user,many=True)
        return JsonResponse(user_serializer.data,safe=False)
    elif request.method=='POST':
        user_data=JSONParser().parse(request)
        user_serializer=UserSerializer(data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add",safe=False)
    elif request.method=='PUT':
        user_data=JSONParser().parse(request)
        course=User.objects.get(_Id=user_data['_id'])
        user_serializer=UserSerializer(course,data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update")
    elif request.method=='DELETE':
        course=User.objects.get(_Id=id)
        course.delete()
        return JsonResponse("Deleted Successfully",safe=False)