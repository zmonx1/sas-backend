from djongo import models

# Create your models here.
class Course(models.Model):
    _id = models.ObjectIdField()
    courseID = models.CharField(max_length=500)
    courseNameTH = models.CharField(max_length=500)
    courseNameEN = models.CharField(max_length=500)
    
class UserType(models.Model):
    _id = models.ObjectIdField()
    userTypeName = models.CharField(max_length=500)
class User(models.Model):
    _id = models.ObjectIdField()
    studentID = models.CharField(max_length=255)
    displayName = models.CharField(max_length=500)
    email = models.CharField(max_length=500)
    password = models.CharField(max_length=500)
    userTypeId = models.CharField(max_length=500)

    
    
    





