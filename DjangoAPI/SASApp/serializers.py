from django.db.models import fields
from rest_framework import serializers
from SASApp.models import Course,User,UserType

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model=Course 
        fields=('_id','courseID','courseNameTH','courseNameEN')\

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('_id','studentID','userTypeId','displayName','email','password')\
            
class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model=UserType
        fields=('_id','userTypeName')\
        
